import click

from app.convertor.api import API
from app.convertor.local_files import local_files as lf


@click.group()
def cli():
    pass

@cli.command()
@click.argument("from_currency")
@click.argument("to_currency")
@click.argument("quantity")
def convert(from_currency, to_currency, quantity):
    c = API.Currency_convertor()
    c.currency_input(from_currency, 'from')
    c.currency_input(to_currency, 'to')
    c.money_input(quantity)
    c.convert()
    result = str(c)
    click.echo(result)

@cli.command()
@click.argument("to_mail")
def send_mail(to_mail):
    lf.send_mail(to_mail)
    click.echo('File is sent.')

if __name__ == '__main__':
    cli()