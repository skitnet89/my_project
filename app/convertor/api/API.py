from app.convertor.api.constants import BASE_CURRENCY, URL
import requests
from app.convertor.api.exceptions import *
import sys

sys.path.insert(1, 'C:\Cur_conv')


class Currency_convertor:

    def __init__(self, url=URL):
        data = requests.get(url).json()
        self.rates = data["rates"]

    def __str__(self):
        result = '{} {} = {} {}'.format(self._money_in, self._from_currency, self._money_out, self._to_currency)
        return result

    def currency_input(self, currency, currency_type):
        currency = currency.upper().strip()
        if self._is_correct_currency(currency):
            if currency_type == 'from':
                self._from_currency = currency
            elif currency_type == 'to':
                self._to_currency = currency
            else:
                raise IncorrectCurrencyType(currency_type)
        else:
            raise IncorrectCurrency(currency)

    def _is_correct_currency(self, currency):
        if currency in self.rates:
            return True
        return False

    def money_input(self, money_amount):
        money_amount = float(money_amount)
        if self._is_correct_quantity(money_amount):
            self._money_in = money_amount
        else:
            raise IncorrectQuantity()

    def _is_correct_quantity(self, quantity):
        if quantity <= 0:
            return False
        return True

    def convert(self):
        if self._from_currency != BASE_CURRENCY:
            result = self._money_in / self.rates[self._from_currency]
        result = round(result * self.rates[self._to_currency], 1)
        self._money_out = result
        # Сохранение
        with open(r'app\convertor\local_files\fail_out.txt', 'w',
                  encoding='utf-8') as fh:  # открываем файл на запись
            fh.write(self.__str__())


if __name__ == '__main__':
    c = Currency_convertor()
    c.currency_input(input('from currency: '), 'from')
    c.currency_input(input('to currency: '), 'to')
    c.money_input(input('money amount: '))
    c.convert()
    print(c)