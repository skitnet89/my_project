class IncorrectCurrency(Exception):
    pass

class IncorrectQuantity(Exception):
    pass

class IncorrectCurrencyType(Exception):
    pass