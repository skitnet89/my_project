import termcolor2
import colorama

from app.convertor.api import API
from app.convertor.api.exceptions import *


def template_test1(test_func):
    def wrapper(test_data_list, test_id):
        test_subid = 0
        c = API.Currency_convertor()
        for test_data in test_data_list:
            test_subid += 1
            if test_func(c, test_data):
                print(termcolor2.colored('Test ' + str(test_id) + '.' + str(test_subid) + ' is passed.', 'green'))
            else:
                print(termcolor2.colored('Test ' + str(test_id) + '.' + str(test_subid) + ' is failed.', 'red'))

    return wrapper


@template_test1
def _test_currency_input1(c, test_data):
    try:
        c.currency_input(test_data, 'from')
        return False
    except IncorrectCurrency:
        return True


@template_test1
def _test_currency_input2(c, test_data):
    try:
        c.currency_input(test_data, 'to')
        return True
    except:
        return False


@template_test1
def _test_currency_type_input(c, test_data):
    try:
        c.currency_input('USD', test_data)
        return False
    except IncorrectCurrencyType:
        return True


@template_test1
def _test_money_input(c, test_data):
    try:
        c.money_input(test_data)
        return False
    except IncorrectQuantity:
        return True
    except ValueError:
        return True


if __name__ == '__main__':
    print()
    colorama.init()
    tests = []
    all_test_data = []

    fl = open('test_api_data.txt')
    for line in fl:
        all_test_data.append(list(line.rstrip('\n').split(',')))
    fl.close()

    keys = list(globals().keys())
    for key in keys:
        if key.startswith('_test_'):
            tests.append(globals()[key])

    if len(tests) == len(all_test_data):
        for i in range(len(tests)):
            tests[i](all_test_data[i], i + 1)
            print()

