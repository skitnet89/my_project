import unittest
import app.convertor.local_files.local_files as lf

class TestStringMethods(unittest.TestCase):
    def test_is_mail(self):
        self.assertTrue(lf.is_mail('123@yandex.ru'))
        self.assertTrue(lf.is_mail('a.sd@yanx.ru'))
        self.assertTrue(lf.is_mail('a.sd@y123anx.ru'))
        self.assertTrue(lf.is_mail('888@321.org'))
        self.assertFalse(lf.is_mail('Adg@google.r9u'))
        self.assertFalse(lf.is_mail('as_d@yandex.ru'))
        self.assertFalse(lf.is_mail('@yandex.ru'))
        self.assertFalse(lf.is_mail('a.sd@ya/nx.ru'))
        self.assertFalse(lf.is_mail('Ad_g@go.ogle.r9u'))

if __name__ == '__main__':
    unittest.main()